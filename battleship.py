#!/usr/bin/env python
# -*- coding:utf-8 -*-

#     ___   _  _____ _____  __   __ __        _____  ___
#    / __\ /_\/__   /__   \/ /  /__/ _\ /\  /\\_   \/ _ \
#   /__\////_\\ / /\/ / /\/ /  /_\ \ \ / /_/ / / /\/ /_)/
#  / \/  /  _  / /   / / / /__//__ _\ / __  /\/ /_/ ___/
#  \_____\_/ \_\/    \/  \____\__/ \__\/ /_/\____/\/
#

import re
import sys
import socket
from thread import *
import time


class Player(object):

    def __init__(self, conn, name, player_num):
        self.conn = conn
        self.name = name
        self.player_num = player_num

    def send_message(self, message):
        self.conn.send(message)


class Game(object):

    def __init__(self, host='', port=50000):
        self.battlefield = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 1, 1, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        ]
        self.count = 0
        self.player_one = None
        self.player_two = None
        self.host = host
        self.port = port
        self.backlog = 2
        self.size = 4096
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind((self.host, self.port))
        self.s.listen(10)
        self.data = ""
        self.active_player = 1

    def __del__(self):
        self.s.shutdown(socket.SHUT_RDWR)
        self.s.close()

    def is_done(self):
        for i in self.battlefield:
            for c in i:
                if c == 1:
                    return False
        return True

    def check_input(self, position):
        t = re.compile('([A-J])([1-9]|(10))$')
        return t.match(position)

    def get_position(self, position):
        i = ord(position[0]) - 17 - 48
        j = int(re.findall('\d+', position)[0]) - 1
        return i, j

    def is_hit(self, i, j):
        try:
            return True if self.battlefield[i][j] == 1 else False
        except IndexError:
            return False

    def fire(self, i, j):
        if self.is_hit(i, j):
            self.battlefield[i][j] = 3
            self.data += u"\nTouche !\n"
        elif self.battlefield[i][j] == 0:
            self.battlefield[i][j] = 2
            self.data += u"\nManque !\n"
        else:
            self.data += u"\nManque !\n"

    def display_board(self):
        self.data += " __________\n"
        for r in xrange(0, 10):
            self.data += '|'
            for c in xrange(0, 10):
                if self.battlefield[r][c] == 1 or self.battlefield[r][c] == 0:
                    self.data += ' '
                elif self.battlefield[r][c] == 2:
                    self.data += 'X'
                else:
                    self.data += 'T'
            self.data += '|\n'
        self.data += " ----------\n"

    def swap_active_player(self):
        if self.active_player == 1:
            self.active_player = 2
        else:
            self.active_player = 1

    def player_thread(self, conn):

        if not self.player_one:
            current_player = self.player_one = Player(conn, "Player 1", 1)
        elif not self.player_two:
            current_player = self.player_two = Player(conn, "Player 2", 2)
        else:
            conn.send("Trop d'utilisateurs sur le serveur !\n")
            conn.close()
            sys.exit()

        conn.send("Bienvenu sur le serveur battleship !\n")
        if current_player.player_num == self.active_player:
            conn.send("Une partie est en cours, commencez en entrant les coordonnees voulues !\n")
        else:
            conn.send("Une partie est en cours, attendez votre tour !\n")
        while True:
            if not self.player_one or not self.player_two:
                conn.send("Attente d'un autre joueur...\n")
                time.sleep(5)
                continue
            if current_player.player_num == self.active_player:
                conn.send("Entrez les coordonnées : ")
                data = conn.recv(1024)
                if not data or "quit" in data:
                    break
                if "close" in data:
                    self.s.close()
                    self.s.shutdown(socket.SHUT_RDWR)
                check = self.play(data[:-2])
                if check:
                    self.swap_active_player()
                conn.sendall(self.data)
                if conn == self.player_one.conn:
                    self.player_two.conn.sendall("\nL'autre joueur a fait un tir en : %s\n" % data[:-2])
                    self.player_two.conn.sendall(self.data)
                    self.player_two.conn.sendall("\nA vous de jouer %s\n" % self.player_two.name)
                    self.player_two.conn.send("Entrez les coordonnées : ")
                else:
                    self.player_one.conn.sendall("\nL'autre joueur a fait un tir en : %s\n" % data[:-2])
                    self.player_one.conn.sendall(self.data)
                    self.player_one.conn.sendall("\nA vous de jouer %s\n" % self.player_two.name)
                    self.player_one.conn.send("Entrez les coordonnées : ")
            else:
                conn.sendall("\nEn attente de l'autre joueur...\n")
                time.sleep(5)
        if conn == self.player_one.conn:
            self.player_one = None
            self.active_player = 2
        else:
            self.player_two = None
            self.active_player = 1
        conn.close()

    def run_serv(self):

        while 1:
            client, address = self.s.accept()
            print 'Connected with ' + address[0] + ':' + str(address[1])
            start_new_thread(self.player_thread, (client,))

    def play(self, position):
        self.data = ""
        play = True
        if not self.check_input(position):
            self.data += "Format de la position incorrecte, attendu : [A-J][1-10]\n"
            play = False
        else:
            i, j = self.get_position(position)
            self.fire(i, j)
            self.count += 1
            if self.is_done():
                self.data += "Felicitation ! Vous avez gagne !\n"
                play = False
        self.display_board()
        return play

if __name__ == "__main__":
    game = Game()
    game.run_serv()