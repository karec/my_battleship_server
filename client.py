#!/usr/bin/env python

"""
A simple echo client
"""

import socket


host = 'localhost'
port = 50000
size = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
while True:
    sending = raw_input("Test : ")
    s.send(sending)
    data = s.recv(size)
    print data